package com.example.mycalculator;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHandler extends SQLiteOpenHelper{
	private static final String DATABASE_NAME = "UserManager";
	private static final String TABLE_NAME = "User";
	private static final String KEY_ID = "id";
	private static final String KEY_FIRSTNAME = "firstname";
	private static final String KEY_LASTNAME = "lastname";
	private static final String KEY_USERNAME = "username";
	
	
	public DatabaseHandler(Context context) {
		super(context, DATABASE_NAME, null, 1);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		String CREATE_TABLE = "CREATE TABLE "+TABLE_NAME+"("+KEY_ID+" INTEGER PRIMARY KEY,"+KEY_FIRSTNAME+" TEXT,"+KEY_LASTNAME+" TEXT,"+KEY_USERNAME+" TEXT"+")";
		db.execSQL(CREATE_TABLE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);

		// Create tables again
		onCreate(db);
	}
	
	public void addUser(String firstname, String lastname, String username) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues value = new ContentValues();
		value.put(KEY_FIRSTNAME, firstname);
		value.put(KEY_LASTNAME, lastname);
		value.put(KEY_USERNAME, username);
		db.insert(TABLE_NAME, null, value);
		db.close();
	}
}
