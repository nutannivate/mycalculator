package com.example.mycalculator;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.TextView;

public class ActivityTwo extends ActionBarActivity{

	private TextView tv_result;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_two);
		
		tv_result = (TextView)findViewById(R.id.tv_result);
	}
	
	public void getData(View v){
		//downlaod json data from internet
		new DownloadData().execute();
	}
	
	public class DownloadData extends AsyncTask<String, String, String>{

		String result="";
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}
		
		@Override
		protected String doInBackground(String... params) {
			HttpClient httpclient = new DefaultHttpClient();
			HttpContext httpcontext = new BasicHttpContext();
			HttpGet httpget = new HttpGet("http://demo.codeofaninja.com/tutorials/json-example-with-php/index.php");
			
			try {
				HttpResponse response = httpclient.execute(httpget, httpcontext);
				
				BufferedReader b_reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
				
				String line = null;
				while((line = b_reader.readLine()) != null){
					result += line + "\n";
				}
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			return result;
		}
		
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			System.out.println(result);
			tv_result.setText(result);
			
			//parse json result and insert values in database
			addUsersInDatabase(result);
		}

		private void addUsersInDatabase(String result) {
			//get instance of database handle class
			DatabaseHandler db = new DatabaseHandler(ActivityTwo.this);
			
			//parse json result
			try {
				JSONArray user_list = new JSONArray(new JSONObject(result).getString("Users"));
				System.out.println(user_list.length());
				//iterate over json array
				for (int i = 0; i < user_list.length(); i++) {
					JSONObject user = new JSONObject(user_list.get(i).toString());
					//add values to database
					db.addUser(user.getString("firstname"), user.getString("lastname"), user.getString("username"));
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
	}
}
