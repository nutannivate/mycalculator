package com.example.mycalculator;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


public class MainActivity extends ActionBarActivity implements OnClickListener{

    private EditText et_num1;
	private EditText et_num2;
	private Button btn_add;
	private Button btn_sub;
	private Button btn_divide;
	private Button btn_multiply;
	private TextView result;
	private Button btn_next;

	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        et_num1 = (EditText)findViewById(R.id.et_num1);
        et_num2 = (EditText)findViewById(R.id.et_num2);
        
        btn_add = (Button)findViewById(R.id.btn_add);
        btn_sub = (Button)findViewById(R.id.btn_sub);
        btn_multiply = (Button)findViewById(R.id.btn_multiply);
        btn_divide = (Button)findViewById(R.id.btn_divide);
        btn_next = (Button)findViewById(R.id.btn_next);
        
        result = (TextView)findViewById(R.id.tv_result);
        
        btn_add.setOnClickListener(this);
        btn_sub.setOnClickListener(this);
        btn_multiply.setOnClickListener(this);
        btn_divide.setOnClickListener(this);
        
        intentFunctions();
    }

    private void intentFunctions() {
    	btn_next.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				//go to next activity
				Intent i = new Intent(MainActivity.this, ActivityTwo.class);
				startActivity(i);
				
				//Open link in browser
//				Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.vogella.com"));
//				startActivity(i);
				
				//send message
//				Intent i = new Intent(Intent.ACTION_SEND);
//				i.setType("text/plain");
//				i.putExtra(android.content.Intent.EXTRA_TEXT,"hello");
//				startActivity(i);
				
				//call functionality
//				Intent i = new Intent(Intent.ACTION_CALL,
//				Uri.parse("tel:(+49)12345789"));
//				startActivity(i);
			}
		});
	}

	@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

	@Override
	public void onClick(View v) {
		float x, y;
		
		if (!TextUtils.isEmpty(et_num1.getText().toString()) && !TextUtils.isEmpty(et_num2.getText().toString())) {
			x = Float.parseFloat(et_num1.getText().toString());
			y = Float.parseFloat(et_num2.getText().toString());
			
			switch (v.getId()) {
			case R.id.btn_add:
				Toast.makeText(MainActivity.this, "Add button clicked", Toast.LENGTH_LONG).show();
				result.setText(x + " + " + y + " = " + (x+y));
				break;
			case R.id.btn_sub:
				Toast.makeText(MainActivity.this, "Sub button clicked", Toast.LENGTH_LONG).show();
				result.setText(x + " - " + y + " = " + (x-y));
				break;
			case R.id.btn_multiply:
				Toast.makeText(MainActivity.this, "Multiply button clicked", Toast.LENGTH_LONG).show();
				result.setText(x + " * " + y + " = " + (x*y));
				break;
			case R.id.btn_divide:
				Toast.makeText(MainActivity.this, "Divide button clicked", Toast.LENGTH_LONG).show();
				result.setText(x + " / " + y + " = " + (x/y));
				break;
			default:
				break;
			}
		} else {
			Toast.makeText(MainActivity.this, "Fields can't be empty", Toast.LENGTH_LONG).show();
		}
		
		
	}
}
